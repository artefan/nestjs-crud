# CRUD

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

![img.png](docs/images/getAll.png)

![img.png](docs/images/createPost.png)

![img.png](docs/images/updatePost.png)

![img.png](docs/images/deletePost.png)

![img.png](docs/images/getPost.png)
