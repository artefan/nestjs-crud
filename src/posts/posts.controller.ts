import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { PostsService } from './posts.service';
import { Post as PostSchemas } from './schemas/post.schema';
import { CreatePostDto, UpdatePostDto } from './dto/post.dto';

@ApiTags('posts')
@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Post()
  @ApiResponse({ status: HttpStatus.CREATED, type: PostSchemas })
  async create(@Body() createPostDto: CreatePostDto) {
    return await this.postsService.create(createPostDto);
  }

  @Get()
  @ApiResponse({ status: HttpStatus.OK, type: [PostSchemas] })
  async findAll(): Promise<PostSchemas[]> {
    return this.postsService.findAll();
  }

  @Get('/:id')
  @ApiResponse({ status: HttpStatus.OK, type: PostSchemas })
  async findById(@Param('id') id) {
    return await this.postsService.getPost(id);
  }

  @Patch('/:id')
  @ApiResponse({ status: HttpStatus.OK, type: PostSchemas })
  async update(@Body() updatePostDto: UpdatePostDto, @Param('id') id) {
    return await this.postsService.update(updatePostDto, id);
  }

  @Delete('/:id')
  @ApiResponse({ status: HttpStatus.OK, description: 'OK' })
  async delete(@Param('id') id) {
    return await this.postsService.delete(id);
  }
}
