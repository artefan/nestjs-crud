import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { Post } from './schemas/post.schema';
import { CreatePostDto, UpdatePostDto } from './dto/post.dto';

@Injectable()
export class PostsService {
  constructor(
    @InjectModel(Post.name) private readonly postModel: Model<Post>,
  ) {}

  async create(createPostDto: CreatePostDto): Promise<Post> {
    try {
      const post = new this.postModel({
        title: createPostDto.title,
        description: createPostDto.description,
        created_at: new Date(),
      });
      return await post.save();
    } catch (error) {
      throw new HttpException('Error creating post', HttpStatus.BAD_REQUEST);
    }
  }

  async update(updatePostDto: UpdatePostDto, postId: string): Promise<Post> {
    try {
      const post = await this.postModel.findById(postId).exec();
      post.title = updatePostDto.title;
      post.description = updatePostDto.description;
      return await post.save();
    } catch (error) {
      throw new HttpException('Error updating post', HttpStatus.BAD_REQUEST);
    }
  }

  async delete(postId: string): Promise<any> {
    try {
      await this.postModel.deleteOne({ _id: postId });
      return 'OK';
    } catch (error) {
      throw new HttpException('Error deleting post', HttpStatus.BAD_REQUEST);
    }
  }

  async getPost(postId: string) {
    const post = await this.postModel.findById(postId).exec();
    if (!post) {
      throw new NotFoundException('Could not find post.');
    }
    return post;
  }

  async findAll(): Promise<Post[]> {
    return await this.postModel.find().select({ __v: 0 }).exec();
  }
}
