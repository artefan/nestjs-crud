import { ApiProperty } from '@nestjs/swagger';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class Post {
  @ApiProperty({
    example: 'What is Lorem Ipsum?',
    description: 'Post title',
  })
  @Prop({ required: true })
  title: string;

  @ApiProperty({
    example:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    description: 'Post description',
  })
  @Prop({ required: true })
  description: string;

  @ApiProperty({
    example: 'Sat Sep 30 2023 20:54:01 GMT+0100 (British Summer Time)',
    description: 'Post created at',
  })
  @Prop({ required: true })
  created_at: string;
}

export const PostsSchema = SchemaFactory.createForClass(Post);
